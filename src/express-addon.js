import express from 'express';
import md5 from 'md5';
import omit from 'lodash.omit';
import path from 'path';
import { parse as urlParse } from 'url';

import Addon from './addon';

/**
 * Class representing an Express Bitbucket Connect add-on
 *
 * The primary purpose of this class is to define an API for
 * binding Connect modules to Express route handlers
 *
 * @extends Addon
 */
class ExpressAddon extends Addon {
  /**
   * Express route handler function
   * @name RouteHandler
   * @function
   * @param {Object} req - Express request object
   * @param {Object} res - Express response object
   * @param {Object} next - Callback function for continuing down middleware chain
   */

  /**
   * Create an instance of an Express Bitbucket Connect add-on
   * @param {Object} props - Descriptor properties
   * @param {Object} [options] - Options hash for Express Bitbucket Connect add-on
   * @param {Logger} options.logger - Custom logger function
   * @param {string} options.routerBaseUrl - The add-on's mount point in your Express app
   * @param {RouteHandler[]} options.middleware - Array of Express middleware for authenticated routes
   */
  constructor(props, options = {}) {
    super(props, options);

    /** @member {Object} router */
    this.router = express.Router(); // eslint-disable-line new-cap
    /** @member {string} routerBaseUrl */
    this.routerBaseUrl = options.routerBaseUrl || '/';
    /** @member {Array} middleware  */
    this.middleware = options.middleware || [];

    this.router.get('/descriptor.json', (req, res) => {
      res.send(this.descriptor);
    });
  }
  /**
   *

  /**
   * Bind lifecycle hook to Express route handler
   * @param {string} url - Express URL for lifecycle hook to POST to
   * @param {string} lifecycle - Name of lifecycle hook, as defined by Connect spec
   * @param {...RouteHandler} handlers - Express route handlers that receive hook
   * @returns {ExpressAddon} The current add-on instance
   */
  registerLifecycleRoute(url, lifecycle, ...handlers) {
    this.registerLifecycle(lifecycle, path.join(this.routerBaseUrl, url));

    if (lifecycle === Addon.Lifecycles.UNINSTALLED) {
      this.router.post(url, ...this.middleware, ...handlers);
    } else {
      this.router.post(url, ...handlers);
    }
    return this;
  }

  /**
   * Generate module properties object with auto-generated URL and
   * key values based on arguments
   * @param {string} url - Express URL pattern
   * @param {Object} props - Connect module properties
   * @param {string} [keyPrefix] - Prefix for module keys in Connect descriptor
   * @returns {Object} The generated set of properties for the Connect module
   */
  generateModuleProps(url, props, keyPrefix = '') {
    if (props.hasOwnProperty('url')) {
      this.log('Module property "url" is automatically generated and will be ignored');
    }

    if (props.hasOwnProperty('key')) {
      this.log('Module property "key" is automatically generated and will be ignored');
    }

    return Object.assign({}, props, {
      url: path.join(this.routerBaseUrl, url.replace(/:([^\/]+)/g, '{$1}')),
      key: keyPrefix + md5(url),
    });
  }

  /**
   * Bind adminPage module to Express route handler
   * @param {string} url - Express URL for module
   * @param {Object} props - Properties of adminPage, as defined by Connect spec
   * @param {...RouteHandler} handlers - Express route handlers that serve module
   * @returns {ExpressAddon} The current add-on instance
   */
  registerAdminPageRoute(url, props, ...handlers) {
    this.registerAdminPage(this.generateModuleProps(url, props, 'admin-page-'));
    this.router.get(urlParse(url).pathname, ...this.middleware, ...handlers);
    return this;
  }

  /**
   * Bind configurePage module to Express route handler
   * @param {string} url - Express URL for module
   * @param {Object} props - Properties of configurePage, as defined by Connect spec
   * @param {...RouteHandler} handlers - Express route handlers that serve module
   * @returns {ExpressAddon} The current add-on instance
   */
  registerConfigurePageRoute(url, props, ...handlers) {
    this.registerConfigurePage(this.generateModuleProps(url, props, 'configure-page-'));
    this.router.get(urlParse(url).pathname, ...this.middleware, ...handlers);
    return this;
  }

  /**
   * Bind fileViews module to Express route handler
   * @param {string} url - Express URL for module
   * @param {Object} props - Properties of fileViews, as defined by Connect spec
   * @param {...RouteHandler} handlers - Express route handlers that serve module
   * @returns {ExpressAddon} The current add-on instance
   */
  registerFileViewsRoute(url, props, ...handlers) {
    this.registerFileViews(this.generateModuleProps(url, props, 'file-views-'));
    this.router.get(urlParse(url).pathname, ...this.middleware, ...handlers);
    return this;
  }

  /**
   * Bind profileTab module to Express route handler
   * @param {string} url - Express URL for module
   * @param {Object} props - Properties of profileTab, as defined by Connect spec
   * @param {...RouteHandler} handlers - Express route handlers that serve module
   * @returns {ExpressAddon} The current add-on instance
   */
  registerProfileTabRoute(url, props, ...handlers) {
    this.registerProfileTab(this.generateModuleProps(url, props, 'profile-tab-'));
    this.router.get(urlParse(url).pathname, ...this.middleware, ...handlers);
    return this;
  }

  /**
   * Bind repoPage module to Express route handler
   * @param {string} url - Express URL for module
   * @param {Object} props - Properties of repoPage, as defined by Connect spec
   * @param {...RouteHandler} handlers - Express route handlers that serve module
   * @returns {ExpressAddon} The current add-on instance
   */
  registerRepoPageRoute(url, props, ...handlers) {
    this.registerRepoPage(this.generateModuleProps(url, props, 'repo-page-'));
    this.router.get(urlParse(url).pathname, ...this.middleware, ...handlers);
    return this;
  }

  /**
   * Bind webItem module to Express route handler
   * @param {string} url - Express URL for module
   * @param {Object} props - Properties of webItem, as defined by Connect spec
   * @param {...RouteHandler} handlers - Express route handlers that serve module
   * @returns {ExpressAddon} The current add-on instance
   */
  registerWebItemRoute(url, props, ...handlers) {
    this.registerWebItem(this.generateModuleProps(url, props, 'web-item-'));
    this.router.get(urlParse(url).pathname, ...this.middleware, ...handlers);
    return this;
  }

  /**
   * Bind webPanel module to Express route handler
   * @param {string} url - Express URL for module
   * @param {Object} props - Properties of webPanel, as defined by Connect spec
   * @param {...RouteHandler} handlers - Express route handlers that serve module
   * @returns {ExpressAddon} The current add-on instance
   */
  registerWebPanelRoute(url, props, ...handlers) {
    this.registerWebPanel(this.generateModuleProps(url, props, 'web-panel-'));
    this.router.get(urlParse(url).pathname, ...this.middleware, ...handlers);
    return this;
  }

  /**
   * Bind webhooks module to Express route handler
   * @param {string} url - Express URL for hook to POST to
   * @param {Object} props - Properties of webhooks, as defined by Connect spec
   * @param {...RouteHandler} handlers - Express route handlers that receive the hook
   * @returns {ExpressAddon} The current add-on instance
   */
  registerWebhooksRoute(url, props, ...handlers) {
    this.registerWebhooks(omit(this.generateModuleProps(url, props), 'key'));
    this.router.post(urlParse(url).pathname, ...this.middleware, ...handlers);
    return this;
  }
}

export default ExpressAddon;
