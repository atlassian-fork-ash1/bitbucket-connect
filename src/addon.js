import isPlainObject from 'lodash.isplainobject';
import values from 'lodash.values';

/**
 * Class representing a Bitbucket Connect add-on
 *
 * The primary purpose of this class is to define an API for building a
 * Bitbucket Connect JSON descriptor. It also provides static properties that
 * define all possible variations of descriptor values.
 */
class Addon {
  /**
   * Function for logging output
   * @name Logger
   * @function
   * @param {string} msg - Log message
   */

  /**
   * Create an instance of a Bitbucket Connect add-on
   * @param {Object} props - Connect descriptor properties
   * @param {Object} [options] - Options hash for add-on instance
   * @param {Logger} options.logger - Custom logger function
   */
  constructor(props = {}, options = {}) {
    /** @member {Object} descriptor */
    this.descriptor = Object.assign({}, props);

    if (typeof options.logger === 'function') {
      /** @member {Logger} logger */
      this.logger = options.logger;
    } else {
      this.logger = console.log; // eslint-disable-line no-console
    }
  }

  /**
   * Log message through either custom logger (if defined) or console.log
   * @param {string} msg - Message to log
   * @returns {Addon} The current add-on instance
   */
  log(msg) {
    this.logger(msg);
    return this;
  }

  /**
   * Get property value from add-on descriptor
   * @param {string} key - The key of the property to fetch
   * @returns {Object|Array|string|number|boolean} The value of the requested property
   */
  get(key) {
    return this.descriptor[key];
  }

  /**
   * Set property value on add-on descriptor
   * @param {string} key - The key of the property to set
   * @param {Object|Array|string|number|boolean} value - The value of the property to set
   * @returns {Addon} The current add-on instance
   */
  set(key, value) {
    if (Array.isArray(value)) {
      this.descriptor[key] = [].concat(value);
    } else if (isPlainObject(value)) {
      this.descriptor[key] = Object.assign({}, value);
    } else {
      this.descriptor[key] = value;
    }
    return this;
  }

  /**
   * Validate and add contexts to add-on descriptor
   * @param {Array} contexts - Array of contexts to be added
   * @returns {Addon} The current add-on instance
   */
  registerContexts(contexts) {
    const VALID_CONTEXTS = values(Addon.Contexts);

    if (!this.descriptor.hasOwnProperty('contexts')) {
      this.descriptor.contexts = [];
    }

    contexts.forEach(context => {
      if (VALID_CONTEXTS.indexOf(context) === -1) {
        this.log(`Context "${context}" is not supported`);
      }
    });

    this.descriptor.contexts = this.descriptor.contexts.concat(contexts);

    return this;
  }

  /**
   * Validate and add API scopes to add-on descriptor
   * @param {Array} scopes - Array of API scopes to be added
   * @returns {Addon} The current add-on instance
   */
  registerScopes(scopes) {
    const VALID_SCOPES = values(Addon.Scopes);

    if (!this.descriptor.hasOwnProperty('scopes')) {
      this.descriptor.scopes = [];
    }

    scopes.forEach(scope => {
      if (VALID_SCOPES.indexOf(scope) === -1) {
        this.log(`Scope "${scope}" is not supported`);
      }
    });

    this.descriptor.scopes = this.descriptor.scopes.concat(scopes);

    return this;
  }

  /**
   * Validate and add a lifecycle hook to add-on descriptor
   * @param {string} lifecycle - Lifecycle hook name to be added
   * @param {string} url - URL the lifecycle hook should POST to
   * @returns {Addon} The current add-on instance
   */
  registerLifecycle(lifecycle, url) {
    const VALID_LIFECYCLES = values(Addon.Lifecycles);

    if (!this.descriptor.hasOwnProperty('lifecycle')) {
      this.descriptor.lifecycle = {};
    }

    if (VALID_LIFECYCLES.indexOf(lifecycle) === -1) {
      this.log(`Lifecycle "${lifecycle}" is not supported`);
    }

    this.descriptor.lifecycle[lifecycle] = url;
    return this;
  }

  /**
   * Validate and add a module definition to add-on descriptor.
   * This method is primarily used to assist other proxy methods
   * in registering different types of modules.
   * @param {string} type - Type of module being registered (webItem, repoPage, etc.)
   * @param {Object} props - Properties of module, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerModule(type, props) {
    const MODULE_TYPES = values(Addon.ModuleTypes);

    if (MODULE_TYPES.indexOf(type) === -1) {
      this.log(`Module type "${type}" is not supported`);
    }

    if (!this.descriptor.hasOwnProperty('modules')) {
      this.descriptor.modules = {};
    }

    if (type === Addon.ModuleTypes.OAUTH_CONSUMER) {
      this.descriptor.modules[type] = Object.assign({}, props);
    } else {
      if (!this.descriptor.modules.hasOwnProperty(type)) {
        this.descriptor.modules[type] = [];
      }
      this.descriptor.modules[type] = this.descriptor.modules[type].concat([Object.assign({}, props)]);
    }

    return this;
  }

  /**
   * Validate and add adminPage to add-on descriptor
   * @param {Object} props - Properties of adminPage, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerAdminPage(props) {
    const LOCATIONS = values(Addon.AdminPageLocations);

    if (LOCATIONS.indexOf(props.location) === -1) {
      this.log(`Location "${props.location}" is not supported for module type "${Addon.ModuleTypes.ADMIN_PAGE}"`);
    }

    this.registerModule(Addon.ModuleTypes.ADMIN_PAGE, props);

    return this;
  }

  /**
   * Validate and add configurePage to add-on descriptor
   * @param {Object} props - Properties of configurePage, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerConfigurePage(props) {
    this.registerModule(Addon.ModuleTypes.CONFIGURE_PAGE, props);
    return this;
  }

  /**
   * Validate and add fileViews to add-on descriptor
   * @param {Object} props - Properties of fileViews, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerFileViews(props) {
    this.registerModule(Addon.ModuleTypes.FILE_VIEWS, props);
    return this;
  }

  /**
   * Validate and add oauthConsumer to add-on descriptor
   * @param {Object} props - Properties of oauthConsumer, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerOauthConsumer(props) {
    this.registerModule(Addon.ModuleTypes.OAUTH_CONSUMER, props);
    return this;
  }


  /**
   * Validate and add profileTab to add-on descriptor
   * @param {Object} props - Properties of profileTab, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerProfileTab(props) {
    this.registerModule(Addon.ModuleTypes.PROFILE_TAB, props);
    return this;
  }

  /**
   * Validate and add repoPage to add-on descriptor
   * @param {Object} props - Properties of repoPage, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerRepoPage(props) {
    const LOCATIONS = values(Addon.RepoPageLocations);

    if (LOCATIONS.indexOf(props.location) === -1) {
      this.log(`Location "${props.location}" is not supported for module type "${Addon.ModuleTypes.REPO_PAGE}"`);
    }

    this.registerModule(Addon.ModuleTypes.REPO_PAGE, props);

    return this;
  }

  /**
   * Validate and add webItem to add-on descriptor
   * @param {Object} props - Properties of webItem, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerWebItem(props) {
    const LOCATIONS = values(Addon.WebItemLocations);

    if (LOCATIONS.indexOf(props.location) === -1) {
      this.log(`Location "${props.location}" is not supported for module type "${Addon.ModuleTypes.WEB_ITEM}"`);
    }

    this.registerModule(Addon.ModuleTypes.WEB_ITEM, props);

    return this;
  }

  /**
   * Validate and add webPanel to add-on descriptor
   * @param {Object} props - Properties of webPanel, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerWebPanel(props) {
    const LOCATIONS = values(Addon.WebPanelLocations);

    if (LOCATIONS.indexOf(props.location) === -1) {
      this.log(`Location "${props.location}" is not supported for module type "${Addon.ModuleTypes.WEB_PANEL}"`);
    }

    // Web panels require a name. If one is not passed in, use the key.
    const propsWithName = Object.assign({ name: { value: props.key } }, props);

    this.registerModule(Addon.ModuleTypes.WEB_PANEL, propsWithName);

    return this;
  }

  /**
   * Validate and add webhooks to add-on descriptor
   * @param {Object} props - Properties of webhooks, as defined by Connect spec
   * @returns {Addon} The current add-on instance
   */
  registerWebhooks(props) {
    this.registerModule(Addon.ModuleTypes.WEBHOOKS, props);

    return this;
  }
}

/**
 * Enum for possible add-on context values
 * @readonly
 * @enum {string}
 */
Addon.Contexts = {
  ACCOUNT: 'account',
  PERSONAL: 'personal',
};

/**
 * Enum for possible API scope values
 * @readonly
 * @enum {string}
 */
Addon.Scopes = {
  ACCOUNT: 'account',
  ACCOUNT_WRITE: 'account:write',
  EMAIL: 'email',
  ISSUE: 'issue',
  ISSUE_WRITE: 'issue:write',
  PROJECT: 'project',
  PROJECT_WRITE: 'project:write',
  PULL_REQUEST: 'pullrequest',
  PULL_REQUEST_WRITE: 'pullrequest:write',
  REPOSITORY: 'repository',
  REPOSITORY_ADMIN: 'repository:admin',
  REPOSITORY_WRITE: 'repository:write',
  SNIPPET: 'snippet',
  SNIPPET_WRITE: 'snippet:write',
  TEAM: 'team',
  TEAM_WRITE: 'team:write',
  WEBHOOK: 'webhook',
  WIKI: 'wiki',
};

/**
 * Enum for possible add-on lifecycle keys
 * @readonly
 * @enum {string}
 */
Addon.Lifecycles = {
  INSTALLED: 'installed',
  UNINSTALLED: 'uninstalled',
};

/**
 * Enum for possible add-on module types
 * @readonly
 * @enum {string}
 */
Addon.ModuleTypes = {
  ADMIN_PAGE: 'adminPages',
  CONFIGURE_PAGE: 'configurePage',
  FILE_VIEWS: 'fileViews',
  OAUTH_CONSUMER: 'oauthConsumer',
  PROFILE_TAB: 'profileTabs',
  REPO_PAGE: 'repoPages',
  WEBHOOKS: 'webhooks',
  WEB_ITEM: 'webItems',
  WEB_PANEL: 'webPanels',
};

/**
 * Enum for possible adminPage module locations
 * @readonly
 * @enum {string}
 */
Addon.AdminPageLocations = {
  ACCOUNT: 'org.bitbucket.account.admin',
  REPOSITORY: 'org.bitbucket.repository.admin',
};

/**
 * Enum for possible repoPage module locations
 * @readonly
 * @enum {string}
 */
Addon.RepoPageLocations = {
  ACTIONS: 'org.bitbucket.repository.actions',
  NAVIGATION: 'org.bitbucket.repository.navigation',
};

/**
 * Enum for possible webItem module locations
 * @readonly
 * @enum {string}
 */
Addon.WebItemLocations = {
  BRANCH_SUMMARY_ACTIONS: 'org.bitbucket.branch.summary.actions',
  BRANCH_SUMMARY_INFO: 'org.bitbucket.branch.summary.info',
  COMMIT_SUMMARY_ACTIONS: 'org.bitbucket.commit.summary.actions',
  COMMIT_SUMMARY_INFO: 'org.bitbucket.commit.summary.info',
  PULL_REQUEST_SUMMARY_ACTIONS: 'org.bitbucket.pullrequest.summary.actions',
  PULL_REQUEST_SUMMARY_INFO: 'org.bitbucket.pullrequest.summary.info',
  REPOSITORY_ACTIONS: 'org.bitbucket.repository.actions',
  REPOSITORY_NAVIGATION: 'org.bitbucket.repository.navigation',
};

/**
 * Enum for possible webPanel module locations
 * @readonly
 * @enum {string}
 */
Addon.WebPanelLocations = {
  REPOSITORY_OVERVIEW_INFORMATION_PANEL: 'org.bitbucket.repository.overview.informationPanel',
};

/**
 * Enum for possible context parameters
 * @readonly
 * @enum {string}
 */
Addon.ContextParameters = {
  REPOSITORY: 'repository',
  BRANCH: 'branch',
  COMMIT: 'commit',
  FILE: 'file',
  TARGET_USER: 'target_user',
  USER: 'user',
  PULLREQUEST: 'pullrequest',
  ISSUE: 'issue',
  SNIPPET: 'snippet',
  AUTH: 'auth',
  ADDON: 'addon',
};

/**
 * Logical grouping of context parameters available for a user
 * @constant
 * @type {string[]}
 */
Addon.TARGET_USER_CONTEXT_PARAMETERS = [
  Addon.ContextParameters.USER,
  Addon.ContextParameters.TARGET_USER,
  Addon.ContextParameters.ADDON,
  Addon.ContextParameters.AUTH,
];

/**
 * Logical grouping of context parameters available for a repository
 * @constant
 * @type {string[]}
 */
Addon.REPO_CONTEXT_PARAMETERS = Addon.TARGET_USER_CONTEXT_PARAMETERS.concat([
  Addon.ContextParameters.REPOSITORY,
]);

/**
 * Logical grouping of context parameters available for a branch
 * @constant
 * @type {string[]}
 */
Addon.BRANCH_CONTEXT_PARAMETERS = Addon.REPO_CONTEXT_PARAMETERS.concat([
  Addon.ContextParameters.BRANCH,
]);

/**
 * Logical grouping of context parameters available for a commit
 * @constant
 * @type {string[]}
 */
Addon.COMMIT_CONTEXT_PARAMETERS = Addon.REPO_CONTEXT_PARAMETERS.concat([
  Addon.ContextParameters.COMMIT,
]);

/**
 * Logical grouping of context parameters available for a pull request
 * @constant
 * @type {string[]}
 */
Addon.PULL_REQUEST_CONTEXT_PARAMETERS = Addon.REPO_CONTEXT_PARAMETERS.concat([
  Addon.ContextParameters.PULLREQUEST,
]);

/**
 * Logical grouping of context parameters available for a file
 * @constant
 * @type {string[]}
 */
Addon.FILE_CONTEXT_PARAMETERS = Addon.COMMIT_CONTEXT_PARAMETERS.concat([
  Addon.ContextParameters.FILE,
  Addon.ContextParameters.COMMIT,
]);

/**
 * Map of module locations and the context parameters available to them
 * @constant
 * @type {Object.<string, Array>}
 */
Addon.LocationContextParameters = {
  [Addon.AdminPageLocations.ACCOUNT]: Addon.TARGET_USER_CONTEXT_PARAMETERS,
  [Addon.AdminPageLocations.REPOSITORY]: Addon.REPO_CONTEXT_PARAMETERS,
  [Addon.RepoPageLocations.ACTIONS]: Addon.REPO_CONTEXT_PARAMETERS,
  [Addon.RepoPageLocations.NAVIGATION]: Addon.REPO_CONTEXT_PARAMETERS,
  [Addon.WebItemLocations.BRANCH_SUMMARY_ACTIONS]: Addon.BRANCH_CONTEXT_PARAMETERS,
  [Addon.WebItemLocations.BRANCH_SUMMARY_INFO]: Addon.BRANCH_CONTEXT_PARAMETERS,
  [Addon.WebItemLocations.COMMIT_SUMMARY_ACTIONS]: Addon.COMMIT_CONTEXT_PARAMETERS,
  [Addon.WebItemLocations.COMMIT_SUMMARY_INFO]: Addon.COMMIT_CONTEXT_PARAMETERS,
  [Addon.WebItemLocations.PULL_REQUEST_SUMMARY_ACTIONS]: Addon.PULL_REQUEST_CONTEXT_PARAMETERS,
  [Addon.WebItemLocations.PULL_REQUEST_SUMMARY_INFO]: Addon.PULL_REQUEST_CONTEXT_PARAMETERS,
  [Addon.WebItemLocations.REPOSITORY_ACTIONS]: Addon.REPO_CONTEXT_PARAMETERS,
  [Addon.WebItemLocations.REPOSITORY_NAVIGATION]: Addon.REPO_CONTEXT_PARAMETERS,
  [Addon.WebPanelLocations.REPOSITORY_OVERVIEW_INFORMATION_PANEL]: Addon.REPO_CONTEXT_PARAMETERS,
};

export default Addon;
