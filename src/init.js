import fs from 'fs';
import inquirer from 'inquirer';
import joi from 'joi';
import path from 'path';
import slugify from 'slug';
import template from 'lodash.template';
import values from 'lodash.values';
import walk from 'walkdir';

import 'shelljs/global';

import Addon from './addon';

const CONTEXTS = values(Addon.Contexts);
const SCOPES = values(Addon.Scopes);
const TEMPLATE_DIR = path.join(__dirname, '..', 'template');

inquirer.prompt([
  {
    name: 'name',
    message: 'Add-on name',
    validate: input => {
      if (!input) {
        return 'Add-on name is required.';
      }
      return true;
    },
  },
  {
    name: 'description',
    message: 'Add-on description',
    validate: input => {
      if (!input) {
        return 'Add-on description is required.';
      }
      return true;
    },
  },
  {
    name: 'vendorName',
    message: 'Vendor name',
    validate: input => {
      if (!input) {
        return 'Vendor name is required.';
      }
      return true;
    },
  },
  {
    name: 'vendorEmail',
    message: 'Vendor email',
    validate: input => {
      if (!input) {
        return 'Vendor email is required.';
      } else if (joi.validate(input, joi.string().email()).error) {
        return 'Vendor email must be a valid email address.';
      }
      return true;
    },
  },
  {
    name: 'vendorUrl',
    message: 'Vendor URL',
    validate: input => {
      if (!input) {
        return 'Vendor URL is required.';
      } else if (joi.validate(input, joi.string().uri()).error) {
        return 'Vendor URL must be a valid URL.';
      }
      return true;
    },
  },
  {
    type: 'list',
    name: 'contexts',
    message: 'In which context should your add-on be installed?',
    choices: CONTEXTS,
  },
  {
    type: 'checkbox',
    name: 'scopes',
    message: 'Which API scopes does your add-on require?',
    choices: SCOPES,
  },
], answers => {
  const templateSettings = { interpolate: /<%=([\s\S]+?)%>/g };
  const key = slugify(answers.name).toLowerCase().slice(0, 80);

  const context = Object.assign({}, answers, {
    key,
    quotedKey: JSON.stringify(key),
    quotedName: JSON.stringify(answers.name),
    quotedDescription: JSON.stringify(answers.description),
    quotedVendorName: JSON.stringify(answers.vendorName),
    quotedVendorUrl: JSON.stringify(answers.vendorUrl),
    quotedAuthor: JSON.stringify(`${answers.vendorName} <${answers.vendorEmail}>`),
    contexts: JSON.stringify([answers.contexts]).replace(/"/g, "'"),
    scopes: JSON.stringify(answers.scopes).replace(/"/g, "'"),
  });

  inquirer.prompt([
    {
      name: 'localPath',
      message: 'Where should we generate the code for your add-on?',
      default: path.join(process.cwd(), key),
      validate: input => {
        try {
          fs.statSync(input);
          return 'Directory or file already exists. Please provide another path.';
        } catch (e) {
          return true;
        }
      },
    },
  ], ({ localPath }) => {
    /* eslint-disable no-undef */

    mkdir(localPath);
    cp('-R', path.join(TEMPLATE_DIR, '*'), localPath);

    walk.sync(localPath, (filePath, stat) => {
      if (stat.isDirectory()) {
        return;
      }

      fs.writeFileSync(filePath, template(fs.readFileSync(filePath), templateSettings)(context));
    });

    cd(localPath);
    mv('babelrc', '.babelrc');
    mv('eslintrc', '.eslintrc');
    mv('gitignore', '.gitignore');

    const eslint = path.join(__dirname, '..', 'node_modules', '.bin', 'eslint');
    const serverPath = path.join(localPath, 'server');
    exec(`${eslint} --fix ${serverPath}`);

    /* eslint-enable no-undef */
  });
});
