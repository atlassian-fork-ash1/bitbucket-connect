# bitbucket-connect

Get started building Bitbucket Connect add-ons with the `bitbucket-connect` scaffolding tool.

## Prerequisites

1. [Install ngrok](https://ngrok.com) and configure it to serve port 3000 (`ngrok 3000`)

## Quick start

`npm install -g bitbucket-connect`

Generate scaffolding for your project:

`bbconnect`

Install your project's dependencies and create a test database:

`cd <project dir>`  
`npm install`  
`npm run migrate`

Run your app using your ngrok URL and OAuth consumer key:

`CONNECT_BASE_URL=<ngrok url> npm run start`

## Registering a Connect module

You'll want to configure your add-on so that you can start displaying
custom content inside of Bitbucket. First create a new file to serve as the
template at `server/views/hello.jade` with the following contents:

```
extends base.jade

block content
  p #{message}
```

Now let's open `server/routes.js` and add a route handler to serve some content:

```javascript
export function helloWorld(req, res) {
  res.render('hello', { message: 'Hello, world!' });
}
```

Next, let's open `server/addon.js` and register a web panel, binding it to
our route handler we just added.

First, import the new route handler you just created:

```javascript
import { helloWorld } from './routes';
```

Then, register your web panel just before the `export default addon;` line:

```javascript
addon.registerWebPanelRoute('/hello-world', {
  location: Addon.WebPanelLocations.REPOSITORY_OVERVIEW_INFORMATION_PANEL,
}, helloWorld);
```

That's it! With these small additions to your add-on, you're now able to
display custom content on the overview page for any repository owned by users
of your add-on.

## Installing your add-on

Let's install your add-on by going to
[Bitbucket](https://bitbucket.org), clicking our avatar in the top-right,
selecting **Bitbucket settings**, then selecting **Manage add-ons** in the 
left sidebar and clicking **Install add-on from URL**.

Your add-on's descriptor is located at `<ngrok url>/descriptor.json`.
Enter the descriptor URL in the text field and click "Install". You will be
prompted to grant the add-on access to your account. Once granted, the add-on 
will be installed.

Go to the overview of one of your repositories to see your add-on in action!


## HTTP client

It is possible to make server-side calls back to the Bitbucket API. First, in addon.js pass the addon to a handler factory function...

    addon.registerFileViewsRoute(
      '/fileView?repoUuid={repository.uuid}&fileCset={file.commit.hash}&filePath={file.path}',
      {...},
      createFileViewHandler(addon)
    );

Then in routes.js define that handler factory method with API calls back to bitbucket...

    export function createFileViewHandler(addon) {
      return (req, res) => {
        const httpClient = req.createHttpClient(addon);
        const { repoUuid, fileCset, filePath } = req.query;
        const url = `/2.0/repositories/{}/${repoUuid}/src/${fileCset}/${filePath}`;
        httpClient.get(url).then(apiRes => {
          const content = transformRawSource(apiRes.data);
          res.render('base', { content });
        });
      };
    }


## API

* [`Addon` class](#markdown-header-addon-class)
* [`ExpressAddon` class](#markdown-header-expressaddon-class)
* [`validateJwtToken` middleware](#markdown-header-validatejwttoken-middleware)

### `Addon` class

* [`constructor(props[, options])`](#markdown-header-constructorprops-options)
* [`registerContexts(contexts)`](#markdown-header-registercontextscontexts)
* [`registerScopes(scopes)`](#markdown-header-registerscopesscopes)
* [`registerLifecycle(lifecycle, url)`](#markdown-header-registerlifecyclelifecycle-url)
* [`registerModule(type, props)`](#markdown-header-registermoduletype-props)
* [`registerAdminPage(props)`](#markdown-header-registeradminpageprops)
* [`registerConfigurePage(props)`](#markdown-header-registerconfigurepageprops)
* [`registerFileViews(props)`](#markdown-header-registerfileviewsprops)
* [`registerOauthConsumer(props)`](#markdown-header-registeroauthconsumerprops)
* [`registerProfileTab(props)`](#markdown-header-registerprofiletabprops)
* [`registerRepoPage(props)`](#markdown-header-registerrepopageprops)
* [`registerWebItem(props)`](#markdown-header-registerwebitemprops)
* [`registerWebPanel(props)`](#markdown-header-registerwebpanelprops)
* [`registerWebhooks(props)`](#markdown-header-registerwebhooksprops)
* [`log(msg)`](#markdown-header-logmsg)
* [`get(key)`](#markdown-header-getkey)
* [`set(key, value)`](#markdown-header-setkey-value)

#### `constructor(props[, options])`

Create an instance of a Bitbucket Connect add-on

* `props` (Object) - Connect descriptor properties
* `[options]` (Object) -  Options hash for add-on instance
    * `options.logger` (Function) - Custom logger function

#### `registerContexts(contexts)`

Validate and add contexts to add-on descriptor

* `contexts` (Array) - Array of contexts to be added

Returns the current add-on instance

#### `registerScopes(scopes)`

Validate and add API scopes to add-on descriptor

* `scopes` (Array) - Array of API scopes to be added

Returns the current add-on instance

#### `registerLifecycle(lifecycle, url)`

Validate and add a lifecycle hook to add-on descriptor

* `lifecycle` (string) - Lifecycle hook name to be added
* `url` (string) - URL the lifecycle hook should POST to

Returns the current add-on instance

#### `registerModule(type, props)`

Validate and add a module definition to add-on descriptor

This method is primarily used to assist other proxy methods in
registering different types of modules

* `type` (string) - Type of module being registered (webItems, repoPages, etc.)
* `props` (Object) - Properties of module, as defined by Connect spec

Returns the current add-on instance

#### `registerAdminPage(props)`

Validate and add adminPage to add-on descriptor

* `props` (Object) - Properties of adminPage, as defined by Connect spec

Returns the current add-on instance

#### `registerConfigurePage(props)`

Validate and add configurePage to add-on descriptor

* `props` (Object) - Properties of configurePage, as defined by Connect spec

Returns the current add-on instance

#### `registerFileViews(props)`

Validate and add fileViews to add-on descriptor

* `props` (Object) - Properties of fileViews, as defined by Connect spec

Returns the current add-on instance

#### `registerOauthConsumer(props)`

Validate and add oauthConsumer to add-on descriptor

* `props` (Object) - Properties of oauthConsumer, as defined by Connect spec

Returns the current add-on instance

#### `registerProfileTab(props)`

Validate and add profileTab to add-on descriptor

* `props` (Object) - Properties of profileTab, as defined by Connect spec

Returns the current add-on instance

#### `registerRepoPage(props)`

Validate and add repoPage to add-on descriptor

* `props` (Object) - Properties of repoPage, as defined by Connect spec

Returns the current add-on instance

#### `registerWebItem(props)`

Validate and add webItem to add-on descriptor

* `props` (Object) - Properties of webItem, as defined by Connect spec

Returns the current add-on instance

#### `registerWebPanel(props)`

Validate and add webPanel to add-on descriptor

* `props` (Object) - Properties of webPanel, as defined by Connect spec

Returns the current add-on instance

#### `registerWebhooks(props)`

Validate and add webhooks to add-on descriptor

* `props` (Object) - Properties of webhooks, as defined by Connect spec

Returns the current add-on instance

#### `log(msg)`

Log message through either custom logger (if defined) or console.log

* `msg` (string) - Message to log

Returns the current add-on instance

#### `get(key)`

Get property value from add-on descriptor

* `key` (string) - The key of the property to fetch

Returns the value of the requested property

#### `set(key, value)`

Set property value on add-on descriptor

* `key` (string) - The key of the property to set
* `value` (\*) - the value of the property to set

Returns the current add-on instance

### `ExpressAddon` class

Class representing an Express Bitbucket Connect add-on

The primary purpose of this class is to define an API for binding
Connect modules to Express route handlers

* [`constructor(props[, options])`](#markdown-header-constructorprops-options_1)
* [`registerLifecycleRoute(url, lifecycle, ...handlers)`](#markdown-header-registerlifecyclerouteurl-lifecycle-handlers)
* [`registerAdminPageRoute(url, props, ...handlers)`](#markdown-header-registeradminpagerouteurl-props-handlers)
* [`registerConfigurePageRoute(url, props, ...handlers)`](#markdown-header-registerconfigurepagerouteurl-props-handlers)
* [`registerFileViewsRoute(url, props, ...handlers)`](#markdown-header-registerfileviewsrouteurl-props-handlers)
* [`registerProfileTabRoute(url, props, ...handlers)`](#markdown-header-registerprofiletabrouteurl-props-handlers)
* [`registerRepoPageRoute(url, props, ...handlers)`](#markdown-header-registerrepopagerouteurl-props-handlers)
* [`registerWebItemRoute(url, props, ...handlers)`](#markdown-header-registerwebitemrouteurl-props-handlers)
* [`registerWebPanelRoute(url, props, ...handlers)`](#markdown-header-registerwebpanelrouteurl-props-handlers)
* [`registerWebhooksRoute(url, props, ...handlers)`](#markdown-header-registerwebhooksrouteurl-props-handlers)
* [`generateModuleProps(url, props[, keyPrefix])`](#markdown-header-generatemodulepropsurl-props-keyprefix)

#### `constructor(props[, options])`

Create an instance of an Express Bitbucket Connect add-on

* `props` (Object) - Connect descriptor properties
* `[options]` (Object) - Options hash for Express Bitbucket Connect add-on
    * `options.logger` (Function) - Custom logger function
    * `options.routerBaseUrl` (string) - The add-on's mount point in your Express app
    * `options.middleware` (Array) - Array of Express middleware for authenticated routes

#### `registerLifecycleRoute(url, lifecycle, ...handlers)`

Bind lifecycle hook to Express route handler

* `url` (string) - Express URL for lifecycle hook to POST to
* `lifecycle` (string) - Name of lifecycle hook, as defined by Connect spec
* `...handlers` (Function) - Express route handlers that receive hook

Returns the current add-on instance

#### `registerAdminPageRoute(url, props, ...handlers)`

Bind adminPage module to Express route handler

* `url` (string) - Express URL for module
* `props` (Object) - Properties of adminPage, as defined by Connect spec
* `...handlers` (Function) - Express route handlers that serve module

Returns the current add-on instance

#### `registerConfigurePageRoute(url, props, ...handlers)`

Bind configurePage module to Express route handler

* `url` (string) - Express URL for module
* `props` (Object) - Properties of configurePage, as defined by Connect spec
* `...handlers` (Function) - Express route handlers that serve module

Returns the current add-on instance

#### `registerFileViewsRoute(url, props, ...handlers)`

Bind fileViews module to Express route handler

* `url` (string) - Express URL for module
* `props` (Object) - Properties of fileViews, as defined by Connect spec
* `...handlers` (Function) - Express route handlers that serve module

Returns the current add-on instance

#### `registerProfileTabRoute(url, props, ...handlers)`

Bind profileTab module to Express route handler

* `url` (string) - Express URL for module
* `props` (Object) - Properties of profileTab, as defined by Connect spec
* `...handlers` (Function) - Express route handlers that serve module

Returns the current add-on instance

#### `registerRepoPageRoute(url, props, ...handlers)`

Bind repoPage module to Express route handler

* `url` (string) - Express URL for module
* `props` (Object) - Properties of repoPage, as defined by Connect spec
* `...handlers` (Function) - Express route handlers that serve module

Returns the current add-on instance

#### `registerWebItemRoute(url, props, ...handlers)`

Bind webItem module to Express route handler

* `url` (string) - Express URL for module
* `props` (Object) - Properties of webItem, as defined by Connect spec
* `...handlers` (Function) - Express route handlers that serve module

Returns the current add-on instance

#### `registerWebPanelRoute(url, props, ...handlers)`

Bind webPanel module to Express route handler

* `url` (string) - Express URL for module
* `props` (Object) - Properties of webPanel, as defined by Connect spec
* `...handlers` (Function) - Express route handlers that serve module

Returns the current add-on instance

#### `registerWebhooksRoute(url, props, ...handlers)`

Bind webhooks module to Express route handler

* `url` (string) - Express URL for hook to POST to
* `props` (Object) - Properties of webhooks, as defined by Connect spec
* `...handlers` (Function) - Express route handlers that receive the hook

Returns the current add-on instance

#### `generateModuleProps(url, props[, keyPrefix])`

Generate module properties object with auto-generated URL and key
values based on arguments

* `url` (string) - Express URL pattern
* `props` (Object) - Connect module properties
* `[keyPrefix]` (string) - Prefix for module keys in Connect descriptor

Returns the generated set of properties for the Connect module

### `validateJwtToken` middleware

Express middleware generator used to validate JWT token

* `getSharedSecret` (Function) - Promise-returning function to retrieve
  shared secret. Accepts client key as its only argument.

Returns Express middleware function