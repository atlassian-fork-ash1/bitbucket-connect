exports.up = function(knex, Promise) {
  return knex.schema.createTableIfNotExists('connections', function (table) {
    table.string('clientKey').unique().index().notNullable();
    table.string('sharedSecret').notNullable();
    table.string('baseUrl').notNullable();
    table.string('baseApiUrl').notNullable();
    table.string('publicKey').notNullable();
    table.string('key').notNullable();
    table.string('productType').notNullable();
    table.json('principal');
    table.json('user');
    table.json('consumer');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('connections');
};
